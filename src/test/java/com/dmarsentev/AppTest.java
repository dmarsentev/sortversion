package com.dmarsentev;

import static com.dmarsentev.App.mySort;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.Arrays;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        App app = new App();
        String [] cv =  new String[]{"1.1.1","2.1.3","3.4.5","5.4.3","1.7.9",
            "1.1.0","1.1","1","1.0","2","2.0"};


        String [] result = mySort(cv);
        String [] expectedResult = new String[]{"1", "1.0", "1.1", "1.1.0", "1.1.1",
            "1.7.9", "2", "2.0", "2.1.3", "3.4.5", "5.4.3"};

        assertTrue( Arrays.equals(result,expectedResult) );
    }
}
