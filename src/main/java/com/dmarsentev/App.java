package com.dmarsentev;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class App
{

    public static void main(String[] args )
    {
        App app = new App();
        String[] cv =  new String[]{"1.1.1","2.1.3","3.4.5","5.4.3","1.7.9",
                "1.1.0","1.1","1","1.0","2","2.0"};


        String [] result = App.mySort( cv );
        System.out.println( Arrays.toString( result ) );


    }

    /**
     * Задача 1.
     * Есть библиотека, версии которой представлены в виде "1.2.3", "2.4.31" и т. д.
     * Т.е. версия состоит из трех чисел, разделенных точками:
     * - Первое число - глобальные изменения.
     * - Второе число - минорные изменения.
     * - Третье число - исправление багов.
     * Нужно написать метод, который на вход принимает массив строк с версиями, а на выходе
     * отдает отсортированные версии, от самой ранней к более поздней.
     *
     *
     * Метод mySort сортирует номера версий.
     * @param array - массив строк String [] с номерами версий
     * @return возвращает отстортированный массив строк String [] с номерами версий
     *
     * Считаем, что версия 1.0 младше версии 1.0.1
     * Считаем, что версия 2 младше версии 2.0
     * Т.е. если все цифры совпадают, но один из номеров версии длиннее,
     * то тот номер версии, который длиннее, считается больше того номера версии, который короче.
     */
    static String[] mySort(String[] array  ) throws RuntimeException {

        List<String> stringList =   Arrays.asList(array);


        for(String s: stringList){
            if( ! s.matches("\\d+(:?\\.\\d+)*") ) throw new RuntimeException("Строка \"" + s + "\" в неверном формате");
        }



        Collections.sort(stringList, (s1, s2) -> {
            String s1arr [] =  s1.split("\\.");
            String s2arr []  = s2.split("\\.");

            Integer [] arr1int = new Integer[s1arr.length];
            Integer [] arr2int = new Integer[s2arr.length];

            for (int i = 0; i < s1arr.length; i++){
                arr1int[i] = Integer.valueOf(s1arr[i]);
            }

            for (int i = 0; i < s2arr.length; i++){
                arr2int[i] = Integer.valueOf(s2arr[i]);
            }

            int maxLength = s1arr.length >= s2arr.length ? s1arr.length : s2arr.length;



            for( int i = 0; i <= maxLength; i++){

                if ( ( i >= arr1int.length ) && (  i >= arr2int.length )  ) return  0;
                if ( ( i <  arr1int.length ) && (  i >= arr2int.length )  ) return  1;
                if ( ( i >= arr1int.length ) && (  i < arr2int.length  )  ) return -1;

                if( arr1int[i] < arr2int[i] ) return -1;
                if( arr1int[i] > arr2int[i] ) return 1;

            }

            return 0;
        });

        return   stringList.toArray(new String[stringList.size()]);

    }


}
